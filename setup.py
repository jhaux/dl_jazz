from setuptools import setup

setup(name='jazz',
    version='0.1',
    description='All the deep learning jazz one needs from time to time',
    author='Johannes Haux',
    author_email='jo.mobile.2011@gmail.com',
    license='MIT',
    packages=['jazz'],
    dependencies=[
        'numpy',
        'pytorch',
        'tqdm',
        'tensorboard_logger'
    ],
    zip_safe=False)
