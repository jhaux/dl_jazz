def clamp(val, min, max):
    if val < min:
        return min
    elif val > max:
        return max
    else:
        return val

def linear(t,
           t0, t1,
           y0, y1,
           cap=True,
           cap_val=None,
           active_intervall=None):
    assert t0 < t1, 't1 must be greater than t0'
    # assert y0 < y1, 'y1 must be greater than y0'

    if cap_val is None:
        cmin, cmax = y0, y1
    elif isinstance(cap_val, list):
        cmin, cmax = cap_val
    else:
        cmin = cmax = cap_val

    if active_intervall is not None:
        assert isinstance(active_intervall, list) \
                and len(active_intervall) % 2 == 0 \
                and len(active_intervall) > 0

        inside_intervall = False
        for start, stop in zip(active_intervall[::2], active_intervall[1::2]):
            inside = t >= start and t < stop
            inside_intervall = inside_intervall or inside

        if not inside_intervall:
            return cmin

    val = (y1 - y0) * (t - t0) / (t1 - t0) + y0

    if cap:
        val = clamp(val, min=cmin, max=cmax)

    return val
