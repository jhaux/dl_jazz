import torch
import torch.nn as nn
import torch.nn.init as init
import torch.optim as optim

from collections import OrderedDict
import numpy as np
import os
import time
from tqdm import tqdm, trange

from jazz.logging import TbLogger as Logger
from jazz.train.checkpoint import save_checkpoint
from jazz.train.project import ProjectManager


def default_initializer(m):
    if isinstance(m, (nn.Conv2d, nn.Linear)):
        try:
            init.xavier_uniform(m.weight)
        except:
            init.xavier_uniform(m.weight_bar)
        m.bias.data.zero_()
    if isinstance(m, nn.LSTMCell):
        init.xavier_uniform(m.weight_ih)
        m.bias_ih.data.zero_()
        init.xavier_uniform(m.weight_hh)
        m.bias_hh.data.zero_()


class BaseTrainer(nn.Module):
    def __init__(self,
                 logging_step=100,
                 checkpoint_steps=500,
                 n_epochs=20,
                 n_steps=200000,
                 log_dir='logs',
                 checkpoint=None,
                 soft_init=True,
                 initializer=default_initializer):
        super().__init__()

        assert n_epochs is not None or n_steps is not None
        assert checkpoint is not None or initializer is not None

        self.n_epochs = n_epochs
        self.n_steps = n_steps

        self.logging_step = logging_step
        self.checkpoint_steps = checkpoint_steps

        self.checkpoint = checkpoint
        self.soft_init = soft_init
        self.initializer = initializer

        self.e_start = 0
        self.train_step_start = 0

        use_date = True
        if self.checkpoint is not None:
            log_dir = self.checkpoint
            path, c_name = os.path.split(self.checkpoint)

            if c_name == 'latest':
                def e_s_key(name):
                    e, s = name.split('_')[0].split('-')
                    return int(e), int(s)
                ckpts = [c for c in os.listdir(path) if 'ckpt.tar' in c]
                c_name = sorted(ckpts, key=e_s_key)[-1]
                log_dir = self.checkpoint = os.path.join(path, c_name)
                print(log_dir)

            e, s = c_name.split('_')[0].split('-')
            self.e_start = int(e) + 1
            self.train_step_start = int(s)

            use_date = False

        self.PM = ProjectManager(log_dir, use_date)

        self.logger = Logger(self.PM.train_dir)

    def forward(self):
        raise NotImplementedError('The trainier is not meant to be used '
                                  'like this')

    def training_step(self, data_batch):
        '''This function needs to be overwritten by the inheriting class. It 
        is later called by fit()'''

        raise NotImplementedError('Do not instanciate the BaseTrainer, but '
                                  'inherit from it and overwrite the '
                                  '`training_step` method')

    def _compatible(self, state_dict):
        '''Test if a state_dict contains exactly the right tensors for
        initialization.
        '''
        if len(state_dict) != len(self.state_dict()):
            return False
        
        keys1 = set(state_dict.keys())
        keys2 = set(self.state_dict().keys())

        common_keys = keys1 & keys2

        if not len(common_keys) == len(keys1) \
                and not len(common_keys) == len(keys2):
            return False

        for k, v1 in state_dict.items():
            v2 = self.state_dict()[k]
            if v1.size() != v3.size():
                return False

        return True

    def initialize(self):
        if self.initializer is not None:
            self.apply(self.initializer)

        if self.checkpoint is not None:
            restore = torch.load(self.checkpoint)
            this_state_dict = tsd = self.state_dict()

            if not self.soft_init and not self._compatible(restore):
                raise ValueError('Chose not soft init, but state dicts do '
                                 'not match.')

            pretrained = {k: v for k, v in restore.items() if k in tsd}
            for k, v in this_state_dict.items():
                size_model = v.size()
                size_load = pretrained[k].size()
                if not size_model == size_load:
                    del pretrained[k]
            this_state_dict.update(pretrained)
            self.load_state_dict(this_state_dict)
            print('Restoring from {}'.format(self.checkpoint))
            print('The following parameters are being restored:')
            for k in sorted(list(pretrained.keys())):
                print(k)
            print('The following parameters are randomly initialized:')
            for k in this_state_dict.keys():
                if k not in pretrained:
                    print(k)

    def fit(self, dataloader):
        if self.n_epochs is None:
            self.n_epochs = int(np.ceil(float(self.n_steps) / float(len(dataloader))))
        else:
            self.n_steps = self.n_epochs * len(dataloader)

        if self.checkpoint_steps == 'epoch' or self.checkpoint_steps is None:
            self.checkpoint_steps = len(dataloader) - 1

        msg = 'Training for {} epochs = {} steps.\n Storing checkpoints every '
        msg += '{} steps at {}, '
        msg += 'starting at epoch {} and step {}'
        msg = msg.format(self.n_epochs,
                         self.n_steps,
                         self.checkpoint_steps, 
                         self.PM.train_dir,
                         self.e_start,
                         self.train_step_start)
        print(msg)

        self.initialize()

        for epoch in trange(self.e_start, self.n_epochs):
            self.epoch = epoch
            for i, batch in enumerate(tqdm(dataloader)):
                self.train_step = i + epoch*len(dataloader)

                step_dict, log_dict = self.training_step(batch)
                self.step(step_dict, log_dict)

                if self.train_step >= self.n_steps:
                    return True
        return True

    def step(self, optim_loss, log_dict=None):
        '''Do all the bookkeeping and updates'''

        l = len(list(optim_loss.keys()))
        for i, (name, loss) in enumerate(optim_loss.items()):
            optimizer = self.optim[name]
            optimizer.zero_grad()

            retain = True if l-1 > i else False
            loss.backward(retain_graph=retain)
            optimizer.step()

        if self.train_step % self.logging_step == 0:
            if log_dict is None:
                log_dict = dict(optim_loss)
                for k, v in log_dict.items():
                    log_dict[k] = ['scalar', v]
            self.log(log_dict)
        else:
            del log_dict

        if self.train_step % self.checkpoint_steps == 0:
            name = os.path.join(self.PM.train_dir,
                                '{}-{}_ckpt.tar'.format(self.epoch,
                                                        self.train_step))
            save_checkpoint(self.state_dict(), False, name)

    def log(self, log_dict, write_to_std=False):
        out = '[{:5}] -'.format(self.train_step)
        for k, [kind, v] in log_dict.items():
            if kind == 'scalar':
                out += ' {}: {:>3.3f}'.format(k, v)
            # log_method = getattr(self.logger, '{}_summary'.format(kind))
            kind = 'value' if kind == 'scalar' else \
                    'histogram' if kind == 'histo' else 'images'
            log_method = getattr(self.logger, 'log_{}'.format(kind))
            if isinstance(v, torch.Tensor):
                v = v.detach().cpu().numpy()
            elif isinstance(v, torch.autograd.Variable):
                v = v.detach().data.cpu().numpy()
            else:
                v = np.array(v)

            log_method(k, v, self.train_step)

        if write_to_std:
            tqdm.write(out)


if __name__ == '__main__':
    print('Testing Base Trainer')
