import os
from datetime import datetime


class ProjectManager(object):

    def __init__(self, root, use_date=False):
        '''Manages directories.

        Arguments:
            root: Path to where to project is supposed to be
        '''

        if '.' in root:
            root, _ = os.path.split(root)

        if 'train' in root or 'eval' in root:
            root, self.mode = os.path.split(root)

        self._root = root

        self.setup(use_date)

    def setup(self, use_date=False):
        if use_date:
            date_str = datetime.now().strftime("%Y-%m-%d %H:%M")
            setup_path = self.root = self._root + '_' + date_str
        else:
            self.root = self._root

        os.makedirs(self.root, exist_ok=True)

        # Training directory
        self.train_dir = os.path.join(self.root, 'train')
        os.makedirs(self.train_dir, exist_ok=True)

        # Eval directory
        self.eval_dir = os.path.join(self.root, 'eval')
        os.makedirs(self.eval_dir, exist_ok=True)
