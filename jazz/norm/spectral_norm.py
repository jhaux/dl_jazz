import torch

from torch import nn
from torch.nn import Parameter


def l2normalize(v, eps=1e-12):
    return v / (v.norm() + eps)


class SpectralNorm(nn.Module):
    '''Implementation by christiancosgrove, found at
    https://github.com/christiancosgrove/pytorch-spectral-normalization-gan
    based on the paper by Miyato et al., found at
    https://openreview.net/forum?id=B1QRgziT-

    Example usage:

    >>> class MyModule(nn.Module):
    >>>     def __init__(self):
    >>>         super().__init__()
    >>>
    >>>         conv1 = nn.Conv2d(3, 32, 3, padding=1)
    >>>         self.snconv1 = SpectralNorm(conv1)
    >>>         fc1 = nn.Linear(32*10*10, 2)
    >>>         self.snfc1 = SpectralNorm(fc1)
    >>>
    >>>
    >>>     def forward(self, x):
    >>>         x = self.snconv1(x).view(-1, 32*10*10)
    >>>         x = self.snfc1(x)
    >>>
    >>>         return x

    And for optimization remember to drop all unwanted paramerters:

    >>> M = MyModule()
    >>> O = torch.optim.Adam(filter(lambda p: p.requires_grad, M.parameters()))
    '''

    def __init__(self, module, name='weight', power_iterations=1):
        super(SpectralNorm, self).__init__()
        self.module = module
        self.name = name
        self.power_iterations = power_iterations
        if not self._made_params():
            self._make_params()

    def _update_u_v(self):
        u = getattr(self.module, self.name + "_u")
        v = getattr(self.module, self.name + "_v")
        w = getattr(self.module, self.name + "_bar")

        height = w.data.shape[0]
        for _ in range(self.power_iterations):
            v.data = l2normalize(torch.mv(torch.t(w.view(height, -1).data),
                                          u.data))
            u.data = l2normalize(torch.mv(w.view(height, -1).data,
                                          v.data))

        # sigma = torch.dot(u.data, torch.mv(w.view(height,-1).data, v.data))
        sigma = u.dot(w.view(height, -1).mv(v))
        setattr(self.module, self.name, w / sigma.expand_as(w))

    def _made_params(self):
        try:
            u = getattr(self.module, self.name + "_u")
            v = getattr(self.module, self.name + "_v")
            w = getattr(self.module, self.name + "_bar")
            return True
        except AttributeError:
            return False

    def _make_params(self):
        w = getattr(self.module, self.name)

        height = w.data.shape[0]
        width = w.view(height, -1).data.shape[1]

        u = Parameter(w.data.new(height).normal_(0, 1), requires_grad=False)
        v = Parameter(w.data.new(width).normal_(0, 1), requires_grad=False)
        u.data = l2normalize(u.data)
        v.data = l2normalize(v.data)
        w_bar = Parameter(w.data)

        del self.module._parameters[self.name]

        self.module.register_parameter(self.name + "_u", u)
        self.module.register_parameter(self.name + "_v", v)
        self.module.register_parameter(self.name + "_bar", w_bar)


    def forward(self, *args):
        self._update_u_v()
        return self.module.forward(*args)


if __name__ == '__main__':
    import numpy as np

    class MyModule(nn.Module):
        def __init__(self):
            super().__init__()

            conv1 = nn.Conv2d(3, 32, 3, padding=1)
            self.snconv1 = SpectralNorm(conv1)

            fc1 = nn.Linear(32*10*10, 2)
            self.snfc1 = SpectralNorm(fc1)

            self.act = nn.ReLU()

        def forward(self, x):
            x = self.snconv1(x)
            x = self.act(x).view(-1, 32*10*10)

            x = self.snfc1(x)
            x = self.act(x)

            return x


    def data(N):
        for i in range(N):
            C = np.random.randint(2)
            if C == 1:
                im = torch.ones(25, 3, 10, 10)
            else:
                im = torch.zeros(25, 3, 10, 10)

            yield im, torch.ones(25).long() * C


    M = MyModule()
    O = torch.optim.Adam(filter(lambda p: p.requires_grad, M.parameters()))
    L = nn.CrossEntropyLoss()
    for im, l in data(100):
        res = M(im)
        loss = L(res, l)

        O.zero_grad()
        loss.backward()
        O.step()

