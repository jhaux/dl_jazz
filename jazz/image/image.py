import numpy as np
from scipy.misc import imresize
import torch


def bbox_center_crop(image,
                     bbox,
                     mode='noise',
                     pad_val=None,
                     mean=None,
                     std=None,
                     rich=False):
    '''Makes a centered crop of an image and pads the sides, if the crop
    edges lie outside the image.

    Arguments:
        image: image to crop [W, H, C], with values in [0, 1]
        bbox: list of in with [x1, y1, x2, y2] coordinates.
        mode: how to fill in the blanks. Either constant or noise.
        pad_val: Value to fill in blanks when mode=constant
        mean: Mean of normal distribution if mode=noise
        std: Standard deviation of normal distribution if mode=noise
        rich: return crop indeces etc

    Returns:
        out_im: cropped image of size [max(width, height)]*2 + [3]
    '''

    is_float = image.max() <= 1

    top_left = np.array([bbox[0], bbox[1]])
    bottom_right = np.array([bbox[2], bbox[3]])

    center = top_left + 0.5*(bottom_right - top_left)

    width = bbox[2] - bbox[0]
    height = bbox[3] - bbox[1]
    crop_size = max(int(width), int(height))

    start = np.array(np.floor(center - 0.5*crop_size), dtype=int)

    size = [crop_size]*2 + [image.shape[-1]]
    if mode == 'constant':
        if pad_val is None:
            pad_val = 0.5 if is_float else 128

        out_im = np.zeros(size) + pad_val
    else:
        if mean is None:
            mean = 0.5 if is_float else 128
        if std is None:
            std = 0.5 if is_float else 128

        out_im = np.random.normal(mean, std, size=size)
        out_im = np.clip(out_im, *([0, 1] if is_float else [0, 256]))

    # slice indices to get crop from image
    sx = max(0, start[0])
    sy = max(0, start[1])
    tx = max(min(start[0] + crop_size, image.shape[1]), 1)
    ty = max(min(start[1] + crop_size, image.shape[0]), 1)

    # slice indices to put slice in out_im
    ssx = max(0, -start[0])
    ssy = max(0, -start[1])
    ttx = max(min(crop_size, image.shape[1] - start[0]), 1)
    tty = max(min(crop_size, image.shape[0] - start[1]), 1)

    crop = image[sy:ty, sx:tx]

    out_im[ssy:tty, ssx:ttx] = crop

    out_im = np.array(out_im, dtype=image.dtype)

    if rich:
        # Some debugging
        return out_im, start, crop_size, \
                [sx, tx, sy, ty], [ssx, ttx, ssy, tty], \
                crop, out_im
    return out_im


def single_image_grid(images, row_images=None, column_images=None):
    n_cols = len(images[0])
    if row_images is not None:
        n_cols += 1

    n_rows = len(images)
    if column_images is not None:
        n_rows += 1

    BS, C, W, H = list(images[0][0].size())
    im_arr = torch.ones(BS, C, H*n_rows, W*n_cols)

    for j in range(n_rows):
        for i in range(n_cols):
            image_index = [i, j]
            if row_images is not None:
                image_index[0] -= 1
            if column_images is not None:
                image_index[1] -= 1

            image_index = tuple(image_index)

            choice = (slice(None, None), 
                      slice(None, None),
                      slice(j*H, (j+1)*H),
                      slice(i*W, (i+1)*W))

            if i == j == 0:
                continue
            elif i == 0:
                if row_images is not None:
                    im = row_images[j-1]
                else:
                    im = images[image_index]
            elif j == 0:
                if column_images is not None:
                    im = column_images[i-1]
                else:
                    im = images[image_index]
            else:
                im_i, im_j = image_index[::-1]
                im = images[im_i][im_j]

            im_arr[choice] = im

    return im_arr


def print_image_to_stdout(image):
    rows, columns = os.popen('stty size', 'r').read().split()

    out_shape = [int(columns)//2, int(columns)]
    image = imresize(image, out_shape)

    image = np.array(image, dtype=float) / 255.

    out_str = ''
    for row in image:
        for color in row:
            out_str += col_to_ascii(color)
        out_str += '\n'
    print(out_str)


def col_to_ascii(color):
    '''Prints a block in a rgb color.'''

    red = [1, 0, 0]
    green = [0, 1, 0]
    blue = [0, 0, 1]
    yellow = [1, 1, 0]
    cyan = [0, 1, 1]
    magenta = [1, 0, 1]
    black = [0, 0, 0]
    white = [1, 1, 1]

    colors = np.array([
            black,
            red,
            green,
            yellow,
            blue,
            magenta,
            cyan,
            white
    ], dtype=float)

    diff = np.linalg.norm(colors - np.expand_dims(np.array(color), 0), axis=-1)
    closest = np.argmin(diff)

    style = ';'.join(['0', str(30+closest), str(40+closest)])
    code = '\x1b[{}m '.format(style, style)

    return code

if __name__ == '__main__':
    # import matplotlib.pyplot as plt
    import os
    import sys
    import scipy.io as sio
    from scipy.misc import imread

    i1 = 0.25 * torch.ones(12, 3, 128, 128)
    i3 = 0.50 * torch.ones(12, 3, 128, 128)
    i2 = 0.75 * torch.ones(12, 3, 128, 128)

    single_image_grid([[i2]*3], [i1], [i3]*3)

    # ims_path = '/export/home/jhaux/data/Penn_Action/frames/0001/'
    # ims = sorted(os.listdir(ims_path))
    # m = sio.loadmat('/export/home/jhaux/data/Penn_Action/labels/0001.mat')

    # idx = 1

    # im = imread(os.path.join(ims_path, ims[idx]))
    # im = np.array(im, dtype=float) / 255.

    # bbox = m['bbox'][idx]

    # crop = bbox_center_crop(im, bbox)

    # print(crop.shape)
    # print(crop.min(), crop.max())

    # print_image_to_stdout(im)

    # print_image_to_stdout(crop)

    # for ix, i in enumerate(ims[-2:-1]):
    #     im = imread(os.path.join(ims_path, i))
    #     im = np.array(im, dtype=float) / 255.

    #     bbox = m['bbox'][ix]

    #     crop = bbox_center_crop(im, bbox)

    #     print_image_to_stdout(crop)

    # # f, [ax1, ax2] = plt.subplots(1, 2)

    # # ax1.imshow(im)
    # # ax2.imshow(crop)
